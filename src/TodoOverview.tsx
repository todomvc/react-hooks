import * as React from "react";
import { TodoItem } from "./TodoItem";
import { ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { ITodo } from "./interfaces";

export interface ITodoOverviewProps {
  todos: ITodo[];
  count?: number;
  completedCount?: number;
  nowShowing?: string;
  toggleAll: (v: boolean) => void;
  toggle: (v: ITodo) => void;
  destroy: (v: ITodo) => void;
  save: (t: ITodo, v: string) => void;
  edit: (id?: string) => void;
  nowEditing?: string;
}

const TodoOverview = (props: ITodoOverviewProps) => {
  const { count, todos } = props;
  if (todos.length === 0) return null;

  const getVisibleTodos = () => {
    return props.todos.filter(todo => {
      switch (props.nowShowing) {
        case ACTIVE_TODOS:
          return !todo.completed;
        case COMPLETED_TODOS:
          return todo.completed;
        default:
          return true;
      }
    });
  };

  const toggleAll = (event: React.FormEvent) => {
    const target: any = event.target;
    const checked = target.checked;
    props.toggleAll(checked);
  };

  const toggle = (todoToToggle: ITodo) => {
    props.toggle(todoToToggle);
  };

  const destroy = (todo: ITodo) => {
    props.destroy(todo);
  };

  const edit = (todo: ITodo) => {
    props.edit(todo.id);
  };

  const save = (todoToSave: ITodo, text: string) => {
    props.save(todoToSave, text);
    props.edit();
  };

  const cancel = () => {
    props.edit();
  };

  return (
    <section className="main">
      <input
        className="toggle-all"
        id="toggle-all"
        type="checkbox"
        onChange={toggleAll}
        checked={count === 0}
      />
      <label htmlFor="toggle-all" />
      <ul className="todo-list">
        {getVisibleTodos().map(todo => (
          <TodoItem
            key={todo.id}
            todo={todo}
            onToggle={() => toggle(todo)}
            onDestroy={() => destroy(todo)}
            onEdit={() => edit(todo)}
            editing={props.nowEditing === todo.id}
            onSave={(v: string) => save(todo, v)}
            onCancel={e => cancel()}
          />
        ))}
      </ul>
    </section>
  );
};

export { TodoOverview };
