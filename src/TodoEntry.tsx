import * as React from "react";
import { ENTER_KEY } from "./Constants";
import ReactDOM from "react-dom";

export interface ITodoEntryProps {
  addTodo: (value: string) => void;
}

const TodoEntry = (props: ITodoEntryProps) => {
  const newField = React.createRef<HTMLInputElement>();
  const handleNewTodoKeyDown = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }

    event.preventDefault();

    let element = ReactDOM.findDOMNode(newField.current) as HTMLInputElement;
    if (element) {
      const val = element.value.trim();
      if (val) {
        props.addTodo(val);
        element.value = "";
      }
    }
  };
  return (
    <input
      ref={newField}
      className="new-todo"
      placeholder="What needs to be done?"
      onKeyDown={handleNewTodoKeyDown}
      autoFocus={true}
    />
  );
};

export { TodoEntry };
