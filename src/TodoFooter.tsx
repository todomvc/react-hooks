import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS } from "./Constants";
import { Utils } from "./Utils";
import * as React from "react";

export interface ITodoFooterProps {
  completedCount: number;
  onClearCompleted: any;
  nowShowing?: string;
  count: number;
}

const TodoFooter = (props: ITodoFooterProps) => {
  const renderFilterLink = (
    filterName: string,
    url: string,
    caption: string
  ) => {
    const { nowShowing } = props;
    return (
      <li>
        <a
          href={"#/" + url}
          className={filterName === nowShowing ? "selected" : ""}
        >
          {caption}
        </a>{" "}
      </li>
    );
  };

  const clearCompleted = () => {
    props.onClearCompleted();
  };

  const { count, completedCount } = props;
  if (!count && !completedCount) return null;

  const activeTodoWord = Utils.pluralize(count, "item");

  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{count}</strong> {activeTodoWord} left
      </span>
      <ul className="filters">
        {renderFilterLink(ALL_TODOS, "", "All")}
        {renderFilterLink(ACTIVE_TODOS, "active", "Active")}
        {renderFilterLink(COMPLETED_TODOS, "completed", "Completed")}
      </ul>
      {completedCount === 0 ? null : (
        <button className="clear-completed" onClick={clearCompleted}>
          Clear completed
        </button>
      )}
    </footer>
  );
};

export { TodoFooter };
