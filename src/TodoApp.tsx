import React, { useEffect, useState } from "react";
import { Router } from "director/build/director";
import { TodoFooter } from "./TodoFooter";
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { ITodoModel } from "./interfaces";
import "./todomvc-app.css";
import "./todomvc-common.css";
import { TodoOverview } from "./TodoOverview";
import { TodoEntry } from "./TodoEntry";

export interface IAppProps {
  model: ITodoModel;
}

const TodoApp = (props: IAppProps) => {
  const [editing, changeEditing] = useState("");
  const [nowShowing, changeFilter] = useState(ALL_TODOS);

  useEffect(
    () => {
      const router = new Router({
        "/": () => changeFilter(ALL_TODOS),
        "/active": () => changeFilter(ACTIVE_TODOS),
        "/completed": () => changeFilter(COMPLETED_TODOS)
      });
      router.init("/");
    },
    [nowShowing]
  );

  const todos = props.model.todos;

  const activeTodoCount = todos.reduce(function(accum, todo) {
    return todo.completed ? accum : accum + 1;
  }, 0);

  const completedCount = todos.length - activeTodoCount;

  return (
    <div>
      <header className="header">
        <h1>todos</h1>
        <TodoEntry addTodo={(v: string) => props.model.addTodo(v)} />
      </header>
      <TodoOverview
        todos={props.model.todos}
        count={activeTodoCount}
        completedCount={completedCount}
        nowShowing={nowShowing}
        toggleAll={v => props.model.toggleAll(v)}
        toggle={v => props.model.toggle(v)}
        destroy={v => props.model.destroy(v)}
        save={(t, v) => props.model.save(t, v)}
        edit={id => changeEditing(id as string)}
        nowEditing={editing}
      />
      <TodoFooter
        count={activeTodoCount}
        completedCount={completedCount}
        nowShowing={nowShowing}
        onClearCompleted={() => props.model.clearCompleted()}
      />
    </div>
  );
};

export { TodoApp };
