import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { ENTER_KEY, ESCAPE_KEY } from "./Constants";
import { ITodo } from "./interfaces";

export interface ITodoItemProps {
  key: string;
  todo: ITodo;
  editing?: boolean;
  onSave: (val: any) => void;
  onDestroy: () => void;
  onEdit: () => void;
  onCancel: (event: any) => void;
  onToggle: () => void;
}

const TodoItem = (props: ITodoItemProps) => {
  const [editText, changeText] = useState(props.todo.title);

  const editField = React.createRef<HTMLInputElement>();

  const handleSubmit = (event: React.FormEvent) => {
    const val = editText.trim();
    if (val) {
      props.onSave(val);
      changeText(val);
    } else {
      props.onDestroy();
    }
  };

  const handleEdit = () => {
    props.onEdit();
    changeText(props.todo.title);
  };
  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.keyCode === ESCAPE_KEY) {
      changeText(props.todo.title);
      props.onCancel(event);
    } else if (event.keyCode === ENTER_KEY) {
      handleSubmit(event);
    }
  };

  const handleChange = (event: React.FormEvent) => {
    const input: any = event.target;
    changeText(input.value);
  };

  useEffect(
    () => {
      const node = ReactDOM.findDOMNode(editField.current) as HTMLInputElement;
      node.focus();
      node.setSelectionRange(node.value.length, node.value.length);
    },
    [props.editing, props.todo, props.editing]
  );

  return (
    <li
      className={classNames({
        completed: props.todo.completed,
        editing: props.editing
      })}
    >
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.todo.completed}
          onChange={props.onToggle}
        />
        <label onDoubleClick={e => handleEdit()}>{props.todo.title}</label>
        <button className="destroy" onClick={props.onDestroy} />
      </div>
      <input
        ref={editField}
        className="edit"
        value={editText}
        onBlur={e => handleSubmit(e)}
        onChange={e => handleChange(e)}
        onKeyDown={e => handleKeyDown(e)}
      />
    </li>
  );
};

export { TodoItem };
